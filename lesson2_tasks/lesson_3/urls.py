from django.urls import path
from . import views
from lesson_3.post_view import index_post, post_page
#from . import post_view

urlpatterns = [
    path('main/', views.main),
    path('main/text/', views.text, name='text'),
    path('main/file/', views.file, name='file'),
    path('main/redirect/', views.redirect, name='redirect'),
    path('main/json', views.json, name='json'),

    # Додав urls для пошуку в’юєшек з тегами.
    path('post/', index_post, name='post'),
    path('post/<int:number>/', post_page, name='posts_list'),
]