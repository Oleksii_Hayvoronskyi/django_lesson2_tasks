from django.http import HttpResponse, FileResponse, HttpResponseRedirect, \
    JsonResponse
from django.templatetags.static import static
from django.shortcuts import render
from django.template import loader


# Create your views here.
def main(request):
    # Поверне користувачу кнопку.
    # return HttpResponse('<button>Click me</button>')

    # Використовую для відображень (представлений).
    # return render(request, 'main.html')

    # ШАБЛОНИ
    # Тестую метод GET.
    # test_temp = loader.get_template(template_name='new_templates.html')
    # return HttpResponse(test_temp.render())

    # Тестую метод SELECT.
    # test_temp_list = loader.select_template(template_name_list=
    #                                    ['test_1',  # Ці шаблони не знаходить, тому що їх не існую.
    #                                     'test_2',  # Шукає той, який є, тобто перебирає з усіх, які може згайтию
    #                                     'new_templates.html'])
    # return HttpResponse(test_temp_list.render())

    # Тестую метод render_to_string.
    test_temp_rend = loader.render_to_string('new_templates.html',
                                               context={'name': 'Vitali',
                                                        'surname': 'KLITSCHKO',
                                                        'int': 51})

    return HttpResponse(test_temp_rend)


def text(request):
                                                    # Статус-код = 207.
    return HttpResponse('<h2>This is Django, baby!</h2>', status=207)


def file(request):
                                   # Тип читання картинки - побайтовий (rb+).
    return FileResponse(open(static('img/tank.jpg'), 'rb+'))


def redirect(request):
    # Зазаначив сайт, куди буде перенаправлятися.
    return HttpResponseRedirect('https://www.ukr.net/')


def json(request):
    # return JsonResponse({i: i + i for i in range(1, 20)}, safe=False)
    # Json-словник
    my_list = [{'priority': 100, 'task': 'Составить список дел'},
               {'priority': 150, 'task': 'Изучать Django'},
               {'priority': 1, 'task': 'Подумать о смысле жизни'}]

    return JsonResponse(my_list, safe=False)