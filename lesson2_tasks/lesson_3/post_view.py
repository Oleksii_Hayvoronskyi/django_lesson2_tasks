from django.http import HttpResponse
from django.template import loader
from django.views.generic import TemplateView

# Створюю в’юшку для тестування основних тегів Django


# Функція для визначення ідексів питань.
def index_post(request) -> HttpResponse:
    latest_question_list = [
        {'id': 1, 'question_text': 'Who am I?'},
        {'id': 2, 'question_text': "Who are you?"},
        {'id': 3, 'question_text': 'Who are we?'},
        {'id': 4, 'question_text': None},  # Застосував для фільтру default
    ]
    template = loader.get_template('post_page.html')
    context = {'latest_question_list': latest_question_list}
    return HttpResponse(template.render(context, request))


# Функція для відповідей на зазначені питання.
def post_page(request, number) -> HttpResponse:
    if number == 1:
        return HttpResponse('Громадянин світу, який живе за своїми правилами.'
                            'Я і тільки я маю право себе засуджувати, '
                            'вихваляти і ненавидіти.')
    elif number == 2:
        return HttpResponse('Ти той, хто поводиться, як дурень, коли бачить '
                            'купу зелених баксів, і не знає, що з ними робити '
                            'далі.')
    elif number == 3:
        return HttpResponse('Разом ми нездолання сила, яка задатна вершити '
                            'правосуддя на благо нашого розпутного '
                            'суспільства!')
    else:
        return HttpResponse('Відповідь відсутня!')
