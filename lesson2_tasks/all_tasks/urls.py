from django.urls import path
from . import views


urlpatterns = [
    path('', views.index, name='index'),
    path('index/', views.index, name='index-view'),
    # Зазначаю шлях з параметром <username>.
    path('bio/<username>/', views.bio, name='bio'),
    # Маршрут із зазначенням числа (року), який введе користувач.
    path('year_published/<int:year>/', views.year_published),

    # Маршрут із зазначенням певного числа (року).
    # path('article/2022/', views.special_route_2022),

    # Додав шлях до регулярних виразів
    path('regex/', views.regex, name='regex'),
]