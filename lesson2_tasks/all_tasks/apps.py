from django.apps import AppConfig


class AllTasksConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'all_tasks'
