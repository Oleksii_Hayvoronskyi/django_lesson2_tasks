from django.http import HttpResponse
from django.shortcuts import render
# from django.http import HttpResponse
# Create your views here.


def index(request):
    return render(request, 'index.html')
    # return HttpResponse("Django - lesson 2")


def bio(request, username):
    print(username)
    return render(request, 'index.html')
    # return HttpResponse("Oleksii Hayvoronskyi")


# def special_route_2022(request):
#     print(request)
#                         # Текст, який повернеться користувачу.
#     return HttpResponse('This function --> is a special_route')


def year_published(request, year):
    print(request)
    print(year)
    print(type(year))
    if year == 2022:
        return HttpResponse('This function --> is a special_route')
    # Поверне число (рік), яке написав користувач.
    return HttpResponse(f'{year}')


# Функція, яка виведе список регуляних виразів.
def regex(request):
    print(request)
    return render(request, 'index.html')
